
static:
	dune build main.exe --profile static --verbose ; \
		dune build dyn.cmxs --profile dev --verbose ; \
		cd _build/default ; \
		./main.exe ; \
		ldd main.exe || true ; \
		readelf --dyn-syms main.exe ; \
		ocamlobjinfo dyn.cmxs ; \
		cd -

dyn:
	dune build main.exe --profile dev --verbose ; \
		dune build dyn.cmxs --profile dev --verbose ; \
		cd _build/default ; \
		./main.exe ; \
		ldd main.exe || true ; \
		readelf --dyn-syms main.exe ; \
		ocamlobjinfo dyn.cmxs ; \
		cd -
