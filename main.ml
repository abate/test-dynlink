let () =
  try
    Dynlink.loadfile "dyn.cmxs";
with Dynlink.Error e ->
  Format.printf "%s@." (Dynlink.error_message e)
